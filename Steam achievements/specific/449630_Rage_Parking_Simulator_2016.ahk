; For Rage Parking Simulator 2016 http://store.steampowered.com/app/449630/
; You'll still have to do the level finish achievements manually if you choose so,
; but this will allow you to farm the 400+ achivements for crashing your car repeatedly
;
; Instructions for use:
; - start level 1
; - crash your car
; - place the mouse cursor on the "restart level" button
; - start the script
; Press F8 to pause/unpause

Pause
Loop{
Send, {Up down} 
Sleep 100
Send, {Up up} 
Sleep 300
Click
Sleep 1000
}
F8::Pause
