; For True or False Universe http://store.steampowered.com/app/698370/
;
; Instructions for use:
; - launch the game in 1920x1080 resolution, full screen (it's important for the script to
;   click at the proper place, although sligthly lower res might work too). Sadly
;   it doesn't seem to be possible to pick your resolution so I did it for my screen size
; - start the game
; - start the script
; Press F8 to pause/unpause
;
; The script always answers "true" and restarts the quiz whenever it goes back to
; the main menu (currently every 3 questions). Getting all achievements should take
; a bit over an hour

Pause
Loop{
Click 950, 945
Sleep 500
Click 500, 780
Sleep 500
}
F8::Pause