; For GooCubelets http://store.steampowered.com/app/397620/
;
; Instructions for use:
; - launch the game in 1280x720 resolution (it's important for the script to click at the proper place)
; - start a new game (ie, start level 1)
; - start the script
; Press F8 to pause/unpause
;
; Unlocking all the "Finish level 1 more time" achievements should take about 70 minutes,
; then you'll have 10 achievements left to do manually

Pause
Loop{
Send, w
Sleep 400
Send, w
Sleep 400
Send, d
Sleep 400
Send, d
Sleep 400
Send, d
Sleep 400
Send, s
Sleep 400
Send, s
Sleep 400
Send, d
Sleep 400
Send, d
Sleep 400
Send, w
Sleep 400
Send, w
Sleep 400
Send, w
Sleep 400
Send, w
Sleep 400
Send, {Escape}
Sleep 500
Click 640, 325
Sleep 500
Click 500, 230
Sleep 1000
}
F8::Pause