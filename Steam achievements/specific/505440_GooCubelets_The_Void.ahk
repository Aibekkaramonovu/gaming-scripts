; For GooCubelets: The Void http://store.steampowered.com/app/505440/
;
; Special thanks to Steam user Foxfire, who made this script http://steamcommunity.com/id/ratmonkey
;
; Instructions for use:
; - load the script (it starts paused)
; - in the main menu, place the mouse cursor right on the bottom-right of the "E" of "NEW GAME"
; - without moving the cursor, click once
; - unpause the script (F8)
; Press F8 to pause/unpause
; NB: for the script to work, the mouse cursor must be placed so as to hit both "New Game" from the main menu,
;     and "Main Menu" from the ingame menu (accessed via the Escape key while playing a level)

Pause
Loop{
Send, a
Sleep 400
Send, a
Sleep 400
Send, a
Sleep 400
Send, s
Sleep 400
Send, s
Sleep 400
Send, a
Sleep 400
Send, a
Sleep 400
Send, w
Sleep 400
Send, w
Sleep 400
Send, a
Sleep 400
Send, a
Sleep 400
Send, w
Sleep 400
Send, w
Sleep 400
Send, d
Sleep 400
Send, d
Sleep 200
Send, {Escape}
Sleep 500
Click
Sleep 1000
Click 
Sleep 8000
}
F8::Pause