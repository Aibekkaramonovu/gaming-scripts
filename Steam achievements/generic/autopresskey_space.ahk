; Trivial script that hits the space key forever once per second
; and thus can be used for any game that requires repetitive single key presses
; (editing the key or delay is trivial)
;
; The script starts paused: press F8 to pause/unpause

Pause
Loop{
Send, {Space}
Sleep 1000
}
F8::Pause
