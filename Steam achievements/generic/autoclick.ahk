; Initially created for Sisters in hotel http://store.steampowered.com/app/625400/
; But it's actually a trivial script that clicks forever once per second
; and thus can be used for any game that requires repetitive clicking, such as graphical novels
;
; Press F8 to pause/unpause

Pause
Loop{
Click
Sleep 1000
}
F8::Pause
